
/**
 * Overlay controller
 * @type Object
 */
var overlay = {
  init: function() {
    $('#overlay').hide();
  },
  message: function(string) {
    var overlayDiv = $('#overlay');
    overlayDiv.html(string);
    overlayDiv.stop(true, true).show().delay(1000).fadeOut(4000);
  }
};

window.onresize = function() {
  var canvas = document.getElementById('theCanvas');
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  var debugCanvas = document.getElementById('debugCanvas');
  debugCanvas.width = window.innerWidth;
  debugCanvas.height = window.innerHeight;
};

var player, wall;

/**
 * Initialization. Called body onLoad.
 */
function init() {

  // Initialization stuff

  document.body.style.overflow = 'hidden';
  window.onresize();
  Yarn.init('theCanvas');
  Yarn.addUpdateCallback(handleTick);
  overlay.init();

  player = new YarnEntityBuilder().setImage('self.png').setCircleShape(24).setInitialPosition(150, 50).finalize();

  for (var i = 0; i < 20; i++) {
    wall = new YarnEntity();
    wall.makeRoundRect((Math.random() * 100) + 10, (Math.random() * 100) + 10);
    Yarn.addEntity(wall);
    wall.teleport(Math.random() * 1000, Math.random() * 1000);
  }

  var pickup = new YarnHUDElement();
  pickup.setImage('hud.png');
  Yarn.addHUDElement(pickup);
  pickup.setPosition(window.innerWidth * 0.8, window.innerHeight * 0.8);

  new YarnEntityBuilder()
      .setImage('self.png')
      .setCircleShape(24)
      .setInitialPosition(Math.random() * 1000, Math.random() * 1000)
      .setPhysicsType(YarnEntityBuilder.PhysicsType.NONE)
      .finalize();

  Yarn.toggleDebug();

  $('#theCanvas').click(function(e) {
    player.setDestination(e.offsetX + Yarn.camera.position.x, e.offsetY + Yarn.camera.position.y);
  });

  $('#debugCanvas').click(function(e) {
    player.setDestination(e.offsetX + Yarn.camera.position.x, e.offsetY + Yarn.camera.position.y);
  });
}

/**
 * Update function. Called by CreateJS?
 */
function handleTick() {

  // Apply acceleration

  if (player.moveToDestination()) {

  } else {
    if (Yarn.keysPressed.left) {
      player.rotateLeft();
    }
    if (Yarn.keysPressed.right) {
      player.rotateRight();
    }
    if (Yarn.keysPressed.up) {
      player.moveForward();
    }
    if (Yarn.keysPressed.down) {
      player.moveBackward();
    }
  }
  if (Yarn.keysPressed.spacebar) {
    overlay.message('You pressed spacebar');
    Yarn.toggleDebug();
  }

  // Apply acceleration to the camera if necessary

  if (player.bitmap.x - Yarn.camera.position.x < 64) {
    Yarn.camera.velocity.x -= 1.5;
  } else if (player.bitmap.x - Yarn.camera.position.x > Yarn.stage.canvas.width - 64) {
    Yarn.camera.velocity.x += 1.5;
  }

  if (player.bitmap.y - Yarn.camera.position.y < 64) {
    Yarn.camera.velocity.y -= 1;
  } else if (player.bitmap.y - Yarn.camera.position.y > Yarn.stage.canvas.height - 64) {
    Yarn.camera.velocity.y += 1;
  }
}

